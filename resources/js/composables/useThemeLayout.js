import { ref, watchEffect } from "vue";

export default function useThemeLayout() {
    const isLoading = ref(false);

    const rtl = ref(false);
    const darkMode = ref(false);
    const topMenu = ref(false);
    const mainContent = ref("lightMode");

    const savedRtl = localStorage.getItem("rtl");
    const savedDarkMode = localStorage.getItem("darkMode");
    const savedTopMenu = localStorage.getItem("topMenu");
    const savedMainContent = localStorage.getItem("mainContent");

    if (savedDarkMode !== null) {
        darkMode.value = JSON.parse(savedDarkMode);
    }

    if (savedRtl !== null) {
        rtl.value = JSON.parse(savedRtl);
    }

    if (savedTopMenu !== null) {
        topMenu.value = JSON.parse(savedTopMenu);
    }

    if (savedMainContent !== null) {
        mainContent.value = JSON.parse(savedMainContent);
    }

    watchEffect(() => {
        localStorage.setItem("rtl", JSON.stringify(rtl.value));
        localStorage.setItem("darkMode", JSON.stringify(darkMode.value));
        localStorage.setItem("topMenu", JSON.stringify(topMenu.value));
        localStorage.setItem("mainContent", JSON.stringify(mainContent.value));
    });

    function changeLayoutMode(newValue) {
        isLoading.value = true;

        setTimeout(() => {
            isLoading.value = false;
            darkMode.value = newValue;
            if (darkMode.value) {
                mainContent.value = "blackMode";
            } else {
                mainContent.value = "lightMode";
            }
        }, 10);
    }

    function changeRtlMode(newValue) {
        isLoading.value = true;

        setTimeout(() => {
            isLoading.value = false;
            rtl.value = newValue;
        }, 10);
    }

    function changeMenuMode(newValue) {
        isLoading.value = true;

        setTimeout(() => {
            isLoading.value = false;
            topMenu.value = newValue;
        }, 10);
    }

    return {
        isLoading,
        rtl,
        darkMode,
        topMenu,
        mainContent,

        changeLayoutMode,
        changeRtlMode,
        changeMenuMode,
    };
}
