import { createWebHistory, createRouter } from "vue-router";

const routes = [];

const router = createRouter({
    history: createWebHistory(),
    linkExactActiveClass: "active",
    routes,
});

export default router;
