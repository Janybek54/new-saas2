import mitt from "mitt";
import { VueMasonryPlugin } from "vue-masonry/src/masonry.plugin.js";

const emitter = mitt();

export default (app) => {
    app.config.globalProperties.emitter = emitter;
    app.use(VueMasonryPlugin);
};
