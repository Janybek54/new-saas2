import VueApexCharts from "vue3-apexcharts";
import VCalendar from "v-calendar";

export default (app) => {
    app.use(VueApexCharts);
    app.use(VCalendar, {});
};
