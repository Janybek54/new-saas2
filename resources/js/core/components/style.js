import { UserCard } from "@/pages/pages/style.js";
import { CreatePost } from "@/pages/apps/myProfile/overview/timeline/style.js";

export default (app) => {
    [
        {
            name: "UserCard",
            ...UserCard,
        },
        {
            name: "CreatePost",
            ...CreatePost,
        },
    ].map((c) => {
        app.component(`sd${c.name}`, c);
    });
};
