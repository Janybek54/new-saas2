import "./bootstrap";
import "ant-design-vue/dist/antd.less";
// import "../css/app.css";

import { createApp, h } from "vue";
import { createInertiaApp } from "@inertiajs/vue3";
import { resolvePageComponent } from "laravel-vite-plugin/inertia-helpers";
import { ZiggyVue } from "../../vendor/tightenco/ziggy/dist/vue.m";
import Layout from "./layouts/withAdminLayout.vue";
// import router from "./routes/index.js";
import store from "@/vuex/store";

// IMPORT PLUGINS
import useAntDesign from "./core/plugins/ant-design";
import useApexCharts from "./core/plugins/apexcharts";
import useFonts from "./core/plugins/fonts";
import useMaps from "./core/plugins/maps";
import useMasonry from "./core/plugins/masonry";
import useUnicons from "./core/plugins/unicons";
import useCustomComponents from "@/core/components/custom";
// import useCustomStyle from "@/core/components/style";

const appName =
    window.document.getElementsByTagName("title")[0]?.innerText || "Laravel";

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: async (name) => {
        console.log(name);
        let page = await resolvePageComponent(
            `./pages/${name}.vue`,
            import.meta.glob("./pages/**/*.vue")
        );
        if (name.startsWith("Auth/Login") || name.startsWith("Auth/Register")) {
            page.default.layout;
        } else if (name.startsWith("Welcome")) {
            page.default.layout;
        } else {
            page.default.layout ??= Layout;
        }

        return page;
    },
    setup({ el, App, props, plugin }) {
        const _app = createApp({
            render: () => h(App, props),
        });
        _app.use(plugin);
        _app.use(store);
        _app.use(ZiggyVue, Ziggy);
        // _app.use(router);
        useAntDesign(_app);
        useApexCharts(_app);
        useFonts(_app);
        useMaps(_app);
        useMasonry(_app);
        useUnicons(_app);
        useCustomComponents(_app);
        // useCustomStyle(_app);
        _app.mount(el);

        return _app;
    },
    progress: {
        color: "#8231D3",
    },
});
